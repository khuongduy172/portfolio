FROM node:alpine

RUN mkdir -p /usr/src/next-app

WORKDIR /usr/src/next-app

COPY package*.json /usr/src/next-app/

RUN npm install

COPY . /usr/src/next-app

RUN npm run build

CMD ["npm", "start"]

EXPOSE 3000