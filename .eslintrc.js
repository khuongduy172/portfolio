module.exports = {
    root: true,
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: "module",
        ecmaFeatures: {
            jsx: true,
        },
    },
    env: {
        browser: true,
        es2021: true,
    },
    extends: [
        "eslint:recommended",
        "plugin:react/recommended",
        "plugin:@typescript-eslint/recommended",
    ],
    overrides: [],
    parser: "@typescript-eslint/parser",
    plugins: ["react", "@typescript-eslint"],
    rules: {
        "@typescript-eslint/no-unused-vars": "warn",
        "react/react-in-jsx-scope": "off",
        "no-undef": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "react/no-unescaped-entities": "off",
        "react/jsx-filename-extension": [
            1,
            {
                extensions: [".ts", ".tsx", ".js", ".jsx"],
            },
        ],
        "react/jsx-props-no-spreading": "off",
        // "import/extensions": [
        //     "error",
        //     "ignorePackages",
        //     {
        //         js: "never",
        //         jsx: "never",
        //         ts: "never",
        //         tsx: "never",
        //     },
        // ],
        "no-nested-ternary": "off",
        "import/prefer-default-export": "off",
    },
};
