import { NavBar } from "@/components";
import type { NextPage } from "next";
import Head from "next/head";
import { GoogleLogin, useGoogleOneTapLogin } from "@react-oauth/google";

const Home: NextPage = () => {
    // useGoogleOneTapLogin({
    //     onSuccess: credentialResponse => {
    //         console.log(credentialResponse);
    //     },
    //     onError: () => {
    //         console.log("Login Failed");
    //     },
    // });
    return (
        <div className="">
            <Head>
                <title>Duy Nguyen's Portfolio Project</title>
                <meta
                    name="description"
                    content="Duy Nguyen's Portfolio Project"
                />
                <link rel="icon" href="/moon.ico" />
            </Head>
            <NavBar />
            <div className="m-4"></div>
        </div>
    );
};

export default Home;
