import axios, { AxiosRequestConfig } from "axios";
import Cookies from "js-cookie";

const option = {
    baseURL: process.env.BASE_URL,
};

function getToken() {
    return Cookies.get("accessToken");
}

const instance = axios.create(option);

instance.interceptors.request.use((config: AxiosRequestConfig) => {
    const token = getToken();
    if (token && config.headers) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
});
